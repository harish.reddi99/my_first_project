import React, { Component } from 'react';
import './App.css';
import {Link,Switch,Route,withRouter} from 'react-router-dom';
class Projects extends Component {

  constructor(){
    super();

    this.state={abc:0};
  }

  slideLeft(){
      this.setState({abc:this.state.abc-650})
  }
  slideRight(){
    
    this.setState({abc:this.state.abc+650})
}
  render() {
    return (
      <div className="App">
       <div className="parent_div">
       <div className="child_div">
       <nav class="nav nav-pills nav-fill">
       
          <a class="nav-item nav-link name " href="#">Hareesh Palavalli</a>
          <a class="nav-item nav-link mail" href="#">
         <a href="mailto:harish@bitlab.academy" target="_blank"> <div>
            <span className="fa fa-envelope"></span>Harish.reddi99@gmail.com
            </div></a>
          <a href="https://google.com" target="_blank"><div>
            <span className="fa fa-phone"></span>91-9603633123
            </div></a>
          </a>
          <a class="nav-item nav-link about" href="#">About </a>
          <a class="nav-item nav-link skill" href="#"><Link  to="/Skills"> Skills</Link></a>
          <a class="nav-item nav-link projects" href="#"><Link  to="/Projects">Projects</Link></a>
          <a class="nav-item nav-link contact" href="#"><Link  to="/Contact">Contact</Link></a>
      </nav>
      <div class="container projects containerStyle">
       <div className="leftPanel"></div>
       <a href="https://gitlab.com/harish.reddi99" target="_blank"><div className="img_div">
       <a href="https://w3schools.com" target="_blank"><div className="img_div_1"></div></a>
       </div>  </a>
<div className="parent_div_slider">
<div id="main_div1" style={{left:this.state.abc+"px"}}>
 <div className="wrapper">
      <h1 className="number">1</h1>
  <div class="divBlock">
     <span class="a">
       <img src="https://image.tmdb.org/t/p/w185_and_h278_bestv2/8bcpki9GfXdXj9esFpPtlate8v0.jpg "/>
     </span>
     <span class="span1">
        <h4>Project name</h4>
          <p>Description</p>
             <p class="p1">dvdsd</p>
             <br/>
     </span>
     </div>
  </div>
  <div className="wrapper">
      <h1 className="number">2</h1>
  <div class="divBlock">
     <span class="a1">
       <img src="https://image.tmdb.org/t/p/w185_and_h278_bestv2/8bcpki9GfXdXj9esFpPtlate8v0.jpg "/>
     </span>
     <span class="span2">
        <h4>Project name</h4>
          <p>Description</p>
             <p class="p2">dvdsd</p>
             <br/>
     </span>
     </div>
  </div>
  <div className="wrapper">
      <h1 className="number">3</h1>
  <div class="divBlock">
     <span class="a1">
       <img src="https://image.tmdb.org/t/p/w185_and_h278_bestv2/8bcpki9GfXdXj9esFpPtlate8v0.jpg "/>
     </span>
     <span class="span2">
        <h4>Project name</h4>
          <p>Description</p>
             <p class="p2">dvdsd</p>
             <br/>
     </span>
     </div>
  </div>
 </div>
</div>
     <span className="fa fa-chevron-circle-left left" onClick={()=>this.slideRight()}></span>
     <span className="fa fa-chevron-circle-right right" onClick={()=>this.slideLeft()}></span>
      </div>
      </div>
      </div>
       </div>
       
    );
  }
}

export default Projects;
