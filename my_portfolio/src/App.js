import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Link,Switch,Route,withRouter} from 'react-router-dom';
import Home from './home';
import Projects from './projects';
import Skills from './skills';
import Contact from './contact';
class App extends Component {
  render() {
    return (
      <Switch>
      <Route exact path="/" component={Home}/>
      <Route location={{pathname:"Projects"}} path="/Projects" component={Projects}/>
      <Route location={{pathname:"Skills"}} path="/Skills" component={Skills}/>
      <Route location={{pathname:"Contact"}} path="/Contact" component={Contact}/>
    </Switch>
    );
  }
}

export default App;
