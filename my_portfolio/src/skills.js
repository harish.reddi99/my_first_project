import React, { Component } from 'react';
import './App.css';
import {Link,Switch,Route,withRouter} from 'react-router-dom';

class Skills extends Component {
  render() {
    return (
      <div className="App">
       <div className="parent_div">
       <div className="child_div">
      
       <nav class="nav nav-pills nav-fill">
          <a class="nav-item nav-link name " href="#">Hareesh Palavalli</a>
          <a class="nav-item nav-link mail" href="#"><a href="mailto:harish@bitlab.academy" target="_blank"> <div>
            <span className="fa fa-envelope"></span>Harish.reddi99@gmail.com
            </div></a>
          <a href="https://google.com" target="_blank"><div>
            <span className="fa fa-phone"></span>91-9603633123
            </div></a>
          </a> 
          <a class="nav-item nav-link about" href="#">About </a>
           <a class="nav-item nav-link skill" href="#"><Link  to="/Skills">Skills</Link></a>
          <a class="nav-item nav-link projects" href="#"><Link  to="/Projects">Projects</Link></a>
          <a class="nav-item nav-link contact" href="#"><Link  to="/Contact">Contact</Link></a>
      </nav>
      <div class="container skills">
       <div className="leftPanel">
       
       </div>
       <a href="https://gitlab.com/harish.reddi99" target="_blank"><div className="img_div">
       <a href="https://w3schools.com" target="_blank"><div className="img_div_1"></div></a>
       </div>  </a>
       
  
    <h5>Html5<span className="percentage_html">70%</span></h5>
  <div class="progress">
    <div class="progress-bar html" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="20">
    </div>
</div>
<h5>Css3<span className="percentage_css">60%</span></h5>
<div class="progress">
    <div class="progress-bar css" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="20">
    </div>
    
</div>
<h5>Javascript<span className="percentage_js">80%</span></h5>
<div class="progress">
    <div class="progress-bar  javascript" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="20">
    </div>
    
</div>
<h5>React Js<span className="percentage_react">75%</span></h5>
<div class="progress">
    <div class="progress-bar react" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="20">
    </div>
    
</div>
<h5>Node Js<span className="percentage_node">60%</span></h5>
<div class="progress">
    <div class="progress-bar node" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="20">
     
    </div>
    
</div>
<h5>Mongo DB<span className="percentage_mongo">50%</span></h5>
<div class="progress">
    <div class="progress-bar mongo" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="20">
    </div>
    
</div>
<h5>Mysql<span className="percentage_mysql">70%</span></h5>

<div class="progress">
    <div class="progress-bar mysql" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="20">
    </div>
    
</div>    
         
         <div>
          </div>
      </div>
      </div>
      </div>
       </div>
       
    );
  }
}
export default Skills;
