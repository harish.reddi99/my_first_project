function loaded(){
	let ulElement = document.getElementById("usersList");
	console.log(ulElement);
	fetch('https://randomuser.me/api/?results=10').then(function(res){
		// console.log(res.json());
		return res.json();
	}).then(function (result){
		console.log(result);

		for(let i=0; i<result.results.length;i++){
			let li=document.createElement('li');
			let img=document.createElement('img');
			let span=document.createElement('span');
			img.src=result.results[i].picture.medium;
			img.className ="class";
			span.innerHTML = result.results[i].name.first + " " + result.results[i].name.last;
			li.appendChild(img);
			li.appendChild(span);
			ulElement.appendChild(li);
		}
	}).catch(function(err){
		console.log(err);
	});
	console.log("Hello There");
}