function loaded(){
	let division = document.getElementById("mainDiv1");
	console.log(division);
	
	fetch(' https://stumped.herokuapp.com/api/posts').then(function(res){
		
		return res.json();
	 }).then(function (result){
		console.log(result);
	for(let i=0; i<result.length;i++){
            let div=document.createElement('div');
			let img=document.createElement('img');
			let figcaption=document.createElement('figcaption');
			// let span=document.createElement('span');
		    img.src=result[i].mainImage;   
			img.className="imgClass";
			div.className="divClass";
               figcaption.innerHTML=result[i].title;
		    
            div.appendChild(img);
            div.appendChild(figcaption);
            // div.appendChild(span);
            division.appendChild(div);
		}
	}).catch(function(err){
		console.log(err);
	});
}
