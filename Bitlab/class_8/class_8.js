var y=1;
let x=1;

  function (){
	  "use strict"//it forces u to follow rules
	  v=10;// if you dont use any let,var or const then its global scope i.e it is available anywhere
	  console.log(v);
	  let x=1;
	  var y=1;
	  let Z=1; {
	  	let x=2;//let has a block scope i.e. it is available only in the block that its written
	  	var y=2;//var has local scope i.e it is available only in the function that its written
	  	const z=3;//const has ablock scope i.e it is available only in the block that its written and u cant change its value once its defined
	  	console.log(x);//2
	  	console.log(y);//2
	  }
	  if (1){
	  	let p=2;
	  	var c=3;
	  }
	  // console.log(p);
	  console.log(c);
	  console.log(x);//1
	  console.log(y)//2

}
//console.log(c);
//print 5 if its 4.4 and print 4 if its 4.8 without using if-else
//loop object keys using for in loop
//while and do-while//give me atleast one example of while and do-while and switch
//find if given variable is an array or not
//show date in 23 june 2018 10:10:10 pm format