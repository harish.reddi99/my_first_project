function loaded() {
    let ulElement = document.getElementById("dataDiv");
    console.log(ulElement);

    fetch('https://api.themoviedb.org/3/movie/popular?api_key=de95a317ffd5c7d771e43cf0fb7c1bc5&language=en-US&page=1').then(function (res) {
        // console.log(res.json());
        return res.json();
    }).then(function (result) {

        console.log(result);
        for (let i = 14; i < result.results.length; i++) {
            ulElement.innerHTML += '<div class="imgDiv"><span>\
                <img src="https://image.tmdb.org/t/p/w185_and_h278_bestv2' + result.results[i].poster_path + '"></span>\
                <span>\
                    \
                        <h4>' + result.results[i].title + '</h4>\
                        <p class="love">&#9829;' + result.results[i].vote_average + ' </p>\
                    \
                </span></div>';
        }
        console.log("Hello There");
    }).catch(function (err) {
        console.log(err);
    });
}